'use strict';

const Boleto = require('node-boleto').Boleto;
const http   = require('http');
const url = require('url');

function sendBoleto(res,values) {
	const boleto = new Boleto({
		'banco': "santander", // nome do banco dentro da pasta 'banks'
		'data_emissao': new Date(),
		'data_vencimento': new Date(new Date().getTime() + 5 * 24 * 3600 * 1000), // 5 dias futuramente
		'valor': values.value, // R$ 15,00 (valor em centavos)
		'nosso_numero': "1234123567",
		'numero_documento': "123123123123",
		'cedente': "Ingressos.com",
		'cedente_cnpj': "18727053000174", // sem pontos e traços
		'agencia': "3978",
		'codigo_cedente': "6404154", // PSK (código da carteira)
		'carteira': "102",
		'pagador': `Nome : ${values.name}\n CPF : ${values.cpf}` ,
		'local_de_pagamento': "PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO.",
		'instrucoes': "Sr. Caixa, aceitar o pagamento e não cobrar juros após o vencimento."
	});
	boleto.renderHTML(html => {
		res.writeHead(200, {'Content-Type': 'text/html;charset=utf-8'});
		res.end(html);
	});	
}

http.createServer((req,res) => {
	let v = url.parse(req.url).pathname.split('/');
	if(req.url.search("boleto") !== -1){
		sendBoleto(res,{
			value : v[4],
			cpf : v[3],
			name : v[2]
		});
	}	else {
		res.writeHead(404, {'Content-Type': 'text/html;charset=utf-8'});
		res.end("not found");
	}
}).listen(3000,() => {
	console.log("boletos on");
});
