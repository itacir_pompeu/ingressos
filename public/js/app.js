'use strict';

(function () { 
	var forms = document.querySelectorAll("form.buy");
	var allForms = Array.from(forms);
	var req =  new XMLHttpRequest();
	var finish = document.querySelector('#finish');
	var ico = document.querySelector('#card-qtd');
	var card = document.querySelector('#card');

	allForms.forEach(f => {
		f.onsubmit = function (e) {
			e.preventDefault();
			var id = f.children[0].children[1].value;
			var qtd = f.children[0].children[0];
			buy({ id : id, qtd : qtd.value});
			qtd.value = 0;
		};
	});

	function buy(obj) {
		var url = `/card/buy/${obj.id}/${obj.qtd}`;
		req.open("get",url,true);
		req.setRequestHeader("Content-Type", "aplication/json");
		req.send(null);
		req.addEventListener("load",function(){
			if(req.status < 300 && req.readyState === 4){
				var card = JSON.parse(req.response);
				cardQtd(card.itens); 
			}
		},false);
	}	

	function cardQtd(itens) {
		var len = itens.map( i =>  Number(i.amount)).reduce((i,c) => i+=c,0);
		btnBuy(len);
	}
	function btnBuy(len) {
		ico.innerHTML = len;
		if(len > 0) {
			card.className = "btn btn-info";
		}

	}
	(function getCard() {
		var url = "/card/qtd/";
		req.open("get",url,true);
		req.setRequestHeader("Content-Type", "aplication/json");
		req.send(null);
		req.addEventListener("load",function(){
			if(req.status < 300 && req.readyState === 4){
				var len = JSON.parse(req.response);
				btnBuy(len);
			}
		},false);
	})();

	finish.addEventListener("click", function() {
		if(confirm("finazer a compra ?")){
			window.location  = "/";
		}
	});
}());
