package models

import (
	"errors"
	"gopkg.in/mgo.v2/bson"
	"ingressos/app/db"
)

type Event struct {
	Id     bson.ObjectId `json:"id" bson:"_id`
	Title  string        `json:"title" bson:"title"`
	Detail string        `json:"detail" bson:"detail"`
	Date   string        `json:"date" bson:"date"`
}

func (p *Event) Save(id string) (Event, error) {
	if id != "" {
		p.Id = bson.ObjectIdHex(id)
		p.update()
		return p.GetEvent(id), nil
	}
	return p.create()
}
func (p *Event) create() (Event, error) {
	p.Id = bson.NewObjectId()
	session := db.SimpleSession("events")
	err := session.DB("test").C("events").Insert(p)
	defer session.Close()
	return *p, err
}

func (p *Event) update() error {
	oid := &p.Id
	session := db.SimpleSession("events")
	err := session.DB("test").C("events").Update(bson.M{"id": oid}, &p)
	defer session.Close()
	return err
}

func (p *Event) FindByName(title string) []Event {
	var events []Event
	session := db.SimpleSession("events")
	if err := session.DB("test").C("events").Find(
		bson.M{"title": &bson.RegEx{
			Pattern: title, Options: "i"}}).All(&events); err != nil {
		panic(err)
	}
	defer session.Close()
	return events
}

func (p *Event) GetEventsByTag(tag string) []Event {
	var events []Event
	session := db.SimpleSession("events")
	if err := session.DB("test").C("events").Find(bson.M{"tickets": &bson.RegEx{Pattern: tag, Options: "i"}}).All(&events); err != nil {
		panic(err)
	}
	defer session.Close()
	return events
}

func (p *Event) GetEvents() []Event {
	var events []Event
	session := db.SimpleSession("events")
	if err := session.DB("test").C("events").Find(bson.M{}).All(&events); err != nil {
		panic(err)
	}
	defer session.Close()
	return events
}

func (p *Event) GetEvent(id string) Event {
	session := db.SimpleSession("events")
	oid := bson.ObjectIdHex(id)
	if err := session.DB("test").C("events").Find(bson.M{"id": oid}).One(&p); err != nil {
		panic(err)
	}
	defer session.Close()
	return *p
}

func (p *Event) RemoveEvent(id string) (done bool, err error) {
	session := db.SimpleSession("events")
	if !bson.IsObjectIdHex(id) {
		err = errors.New("id invaid")
		return done, err
	}
	oid := bson.ObjectIdHex(id)
	err = session.DB("test").C("events").Remove(bson.M{"id": oid})
	defer session.Close()
	if err == nil {
		done = true
	}
	return done, err
}
