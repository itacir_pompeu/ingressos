package models

import (
	"errors"
	"gopkg.in/mgo.v2/bson"
	"ingressos/app/db"
)

type Nivel string

type Ticket struct {
	Id      bson.ObjectId `json:"id" bson:"_id`
	EventId string        `json:"event_id" bson:"event_id`
	Amount  int64         `json:amout bson:"amout"`
	Cost    float64       `json:"const" bson:"cost" `
	Cat     string        `json:"cat" bson:"cat" `
}

func (p *Ticket) Save(id string) (Ticket, error) {
	if id != "" {
		p.Id = bson.ObjectIdHex(id)
		p.update()
		return p.GetTicket(id), nil
	}
	return p.create()
}

func (p *Ticket) create() (Ticket, error) {
	p.Id = bson.NewObjectId()
	session := db.SimpleSession("tickets")
	err := session.DB("test").C("tickets").Insert(p)
	defer session.Close()
	return *p, err
}

func (p *Ticket) update() error {
	oid := &p.Id
	session := db.SimpleSession("tickets")
	err := session.DB("test").C("tickets").Update(bson.M{"id": oid}, &p)
	defer session.Close()
	return err
}

func (p *Ticket) FindByName(title string) []Ticket {
	var tickets []Ticket
	session := db.SimpleSession("tickets")
	if err := session.DB("test").C("tickets").Find(
		bson.M{"title": &bson.RegEx{
			Pattern: title, Options: "i"}}).All(&tickets); err != nil {
		panic(err)
	}
	defer session.Close()
	return tickets
}

func (p *Ticket) GetTicketsByTag(tag string) []Ticket {
	var tickets []Ticket
	session := db.SimpleSession("tickets")
	if err := session.DB("test").C("tickets").Find(bson.M{"tickets": &bson.RegEx{Pattern: tag, Options: "i"}}).All(&tickets); err != nil {
		panic(err)
	}
	defer session.Close()
	return tickets
}

func (p *Ticket) GetTicketsbyEventId(id string) []Ticket {
	var tickets []Ticket
	session := db.SimpleSession("tickets")
	if err := session.DB("test").C("tickets").Find(bson.M{"eventid": id}).All(&tickets); err != nil {
		panic(err)
	}
	defer session.Close()
	return tickets
}

func (p *Ticket) GetTickets() []Ticket {
	var tickets []Ticket
	session := db.SimpleSession("tickets")
	if err := session.DB("test").C("tickets").Find(bson.M{}).All(&tickets); err != nil {
		panic(err)
	}
	defer session.Close()
	return tickets
}

func (p *Ticket) GetTicket(id string) Ticket {
	session := db.SimpleSession("tickets")
	oid := bson.ObjectIdHex(id)
	if err := session.DB("test").C("tickets").Find(bson.M{"id": oid}).One(&p); err != nil {
		panic(err)
	}
	defer session.Close()
	return *p
}

func (p *Ticket) RemoveTicket(id string) (done bool, err error) {
	session := db.SimpleSession("tickets")
	if !bson.IsObjectIdHex(id) {
		err = errors.New("id invaid")
		return done, err
	}
	oid := bson.ObjectIdHex(id)
	err = session.DB("test").C("tickets").Remove(bson.M{"id": oid})
	defer session.Close()
	if err == nil {
		done = true
	}
	return done, err
}
