package models

import (
	"errors"
	"fmt"
	"github.com/chrislusf/glow/flow"
	"gopkg.in/mgo.v2/bson"
	"ingressos/app/db"
	"math"
)

type Iten struct {
	IdTicket string  `json:"ticket_id" bson:"ticket_id"`
	Amount   int64   `json:"amount" bson:"amount"`
	Cost     float64 `json:"cost" bson:"cost"`
}

type Card struct {
	Id     bson.ObjectId `json:"id" bson:"_id"`
	IdUser bson.ObjectId `json:"user_id" bson:"user_id"`
	Itens  []Iten        `json:"itens" bson:"itens"`
	Total  float64       `json:"total" bson:"total"`
	State  bool          `json:"state" : bson:"state"`
}

func (p *Card) Save() (Card, error) {
	if p.Id.Hex() != "" {
		p.update()
		return p.GetCard(p.Id.Hex()), nil
	}
	return p.create()
}

func (p *Card) create() (Card, error) {
	p.Id = bson.NewObjectId()
	p.calcTotal()
	session := db.SimpleSession("cards")
	err := session.DB("test").C("cards").Insert(p)
	defer session.Close()
	return *p, err
}

func (p *Card) update() error {
	oid := &p.Id
	p.calcTotal()
	session := db.SimpleSession("cards")
	err := session.DB("test").C("cards").Update(bson.M{"_id": oid}, &p)
	defer session.Close()
	return err
}

func (p *Card) CloseCard() (err error) {
	oid := &p.Id

	for _, v := range p.Itens {
		ticket := new(Ticket).GetTicket(v.IdTicket)
		ticket.Amount -= v.Amount
		if t, err := ticket.Save(v.IdTicket); err != nil {
			return fmt.Errorf("error :", err.Error())
		} else {
			fmt.Println(t)
			p.State = false
		}
	}

	session := db.SimpleSession("cards")
	err = session.DB("test").C("cards").Update(bson.M{"_id": oid}, &p)
	defer session.Close()
	return err
}

func (p *Card) FindByName(title string) []Card {
	var cards []Card
	session := db.SimpleSession("cards")
	if err := session.DB("test").C("cards").Find(
		bson.M{"title": &bson.RegEx{
			Pattern: title, Options: "i"}}).All(&cards); err != nil {
		panic(err)
	}
	defer session.Close()
	return cards
}

func (p *Card) GetCardsByTag(tag string) []Card {
	var cards []Card
	session := db.SimpleSession("cards")
	if err := session.DB("test").C("cards").Find(bson.M{"cards": &bson.RegEx{Pattern: tag, Options: "i"}}).All(&cards); err != nil {
		panic(err)
	}
	defer session.Close()
	return cards
}

func (p *Card) GetCards() []Card {
	var cards []Card
	session := db.SimpleSession("cards")
	if err := session.DB("test").C("cards").Find(bson.M{}).All(&cards); err != nil {
		panic(err)
	}
	defer session.Close()
	return cards
}

func (p *Card) GetByUserId(id string) (card Card, err error) {
	session := db.SimpleSession("cards")
	oid := bson.ObjectIdHex(id)
	if err = session.DB("test").C("cards").Find(
		bson.M{"user_id": oid, "state": true}).One(&p); err != nil {
		return *p, err
	}
	defer session.Close()
	return *p, err
}

func (p *Card) GetCard(id string) Card {
	session := db.SimpleSession("cards")
	oid := bson.ObjectIdHex(id)
	if id == "" {
		oid = p.Id
	}
	if err := session.DB("test").C("cards").Find(
		bson.M{"_id": oid}).One(&p); err != nil {
		panic(err)
	}
	defer session.Close()
	return *p
}

func (p *Card) RemoveCard(id string) (done bool, err error) {
	session := db.SimpleSession("cards")
	if !bson.IsObjectIdHex(id) {
		err = errors.New("id invaid")
		return done, err
	}
	oid := bson.ObjectIdHex(id)
	err = session.DB("test").C("cards").Remove(bson.M{"_id": oid})
	defer session.Close()
	if err == nil {
		done = true
	}
	return done, err
}

func (i *Iten) AddCost() {
	t := new(Ticket).GetTicket(i.IdTicket)
	i.Cost = t.Cost
}

func (c *Card) calcTotal() {
	c.Total = toFixed(c.CardReduce(), 2)
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func (c *Card) CardReduce() float64 {
	var total float64
	var qtd int64
	flow.New().Slice(
		c.Itens,
	).Map(func(t Iten) float64 {
		qtd += t.Amount
		total += (t.Cost * float64(t.Amount))
		fmt.Println(qtd)
		return total
	}).Run()
	return total
}
