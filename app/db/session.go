package db

import (
	"gopkg.in/mgo.v2"
)

func SimpleSession(col string) *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost/" + col)
	if err != nil {
		panic(err)
	}
	index := mgo.Index{
		Key:        []string{"email", "cpf"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	c := session.DB("test").C("persons")
	err = c.EnsureIndex(index)
	if err != nil {
		panic(err)
	}
	return session
}
