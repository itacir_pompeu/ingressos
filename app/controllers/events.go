package controllers

import (
	"github.com/revel/revel"
	"ingressos/app/models"
)

type Events struct {
	*revel.Controller
}

func (e Events) Main(id string) revel.Result {
	if id != "" {
		event := new(models.Event).GetEvent(id)
		return e.Render(event)
	}
	return e.Render()
}

func (e Events) Create(event models.Event, id string) revel.Result {
	if event, err := event.Save(id); err != nil {
		e.Flash.Error("erro ao criar o evento " + event.Title)
		return e.Redirect(Events.Main)
	}
	e.Flash.Success(event.Title + " Criado")
	return e.Redirect(App.Index)
}

func (e Events) Buy(id string) revel.Result {
	tickets := new(models.Ticket).GetTicketsbyEventId(id)
	event := new(models.Event).GetEvent(id)
	return e.Render(event, tickets)
}

func (e Events) Remove(id string) revel.Result {
	if gone, _ := new(models.Event).RemoveEvent(id); gone {
		e.Flash.Success("Removido com Successo")
	} else {
		e.Flash.Error("erro ao deletar")
	}
	return e.Redirect(App.Index)
}
