package controllers

import (
	"github.com/revel/revel"
	"ingressos/app/models"
	"regexp"
)

type App struct {
	*revel.Controller
}

func (c App) Index(q string) revel.Result {
	c.Validation.Clear()
	events := new(models.Event).GetEvents()

	if t, _ := regexp.MatchString("^[a-zA-Z]{2,}$", q); t {
		events = new(models.Event).FindByName(q)
	}

	if user := c.connected(); user != nil {
		return c.Render(events, user)
	}
	return c.Render(events)
}
