package controllers

import (
	"bytes"
	"fmt"
	"github.com/revel/revel"
	"ingressos/app/models"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type Card struct {
	App
}

func (e Card) Main() revel.Result {
	user := e.connected()
	card, _ := new(models.Card).GetByUserId(user.Id.Hex())
	if len(card.Itens) == 0 {
		return e.Redirect(App.Index)
	}
	var qtd int64
	for _, v := range card.Itens {
		qtd += v.Amount
	}
	return e.Render(user, card, qtd)
}

func (e Card) GetQtd() revel.Result {
	user := e.connected()
	card, err := new(models.Card).GetByUserId(user.Id.Hex())
	if err != nil || len(card.Itens) == 0 {
		return e.RenderJson("")
	}
	var qtd int64
	for _, v := range card.Itens {
		qtd += v.Amount
	}
	return e.RenderJson(qtd)
}

func (e Card) Create() revel.Result {
	return e.Render()
}

func (e Card) Remove(id string) revel.Result {
	if gone, err := new(models.Card).RemoveCard(id); gone {
		e.Flash.Success("Removido com Successo")
	} else {
		e.Flash.Error("erro ao deletar" + err.Error())
	}
	return e.Redirect(App.Index)
}

//verificar carrinho em aberto
func (e Card) Buy() revel.Result {
	url := strings.Split(e.Request.URL.Path, "/")
	card, _ := e.existCardOpend()
	iten := new(models.Iten)
	amount := url[len(url)-1]
	id := url[len(url)-2]
	if card == nil {
		revel.INFO.Println("não existe carrinho criando")
		if value, err := strconv.ParseInt(amount, 10, 64); err == nil {
			iten.IdTicket = id
			iten.Amount = value
			iten.AddCost()
			card, err := e.createCard(*iten)
			if err != nil {
				return e.NotFound("erro", err.Error())
			}
			return e.RenderJson(card)
		}
	} else {
		revel.INFO.Println("carrinhos existe atulize")
		if value, err := strconv.ParseInt(amount, 10, 64); err == nil {
			iten.IdTicket = id
			iten.Amount = value
			iten.AddCost()
			card.Itens = append(card.Itens, *iten)
			c, _ := card.Save()
			return e.RenderJson(c)
		}
	}
	return e.NotFound("erro", "same error")
}

func (e Card) Finish(id string) revel.Result {
	if id == "" {
		return e.Redirect(App.Index)
	}
	user := e.connected()
	card := new(models.Card).GetCard(id)
	card.CloseCard()
	total := fmt.Sprint(card.Total)

	url := "http://localhost:3000/boleto/" + strings.Split(user.Name, " ")[0] + "/" + user.Cpf + "/" + total + "00"
	resp, err := http.Get(url)
	if err == nil {
		body, _ := ioutil.ReadAll(resp.Body)
		buffer := bytes.NewBuffer(body)
		return e.RenderHtml(buffer.String())
	}
	return e.Render(card, user)
}

func (e Card) createCard(iten models.Iten) (card *models.Card, err error) {
	var itens []models.Iten
	itens = append(itens, iten)
	card = new(models.Card)
	if user := e.connected(); user != nil {
		card.IdUser = user.Id
		card.Itens = itens
		card.State = true
	}
	c, err := card.Save()
	return &c, err
}

func (e Card) existCardOpend() (*models.Card, *models.Person) {
	user := e.connected()
	if user != nil {
		card, err := new(models.Card).GetByUserId(user.Id.Hex())
		if err == nil && card.State {
			return &card, user
		}
	}
	return nil, nil
}
