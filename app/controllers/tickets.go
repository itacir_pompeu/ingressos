package controllers

import (
	"github.com/revel/revel"
	"ingressos/app/models"
	"strconv"
)

type Tickets struct {
	*revel.Controller
}

func (e Tickets) Main() revel.Result {
	events := new(models.Event).GetEvents()
	return e.Render(events)
}

func (e Tickets) Create() revel.Result {
	values := e.Params.Query
	var ticket models.Ticket
	if v, err := strconv.ParseFloat(values["cost"][0], 64); err == nil {
		ticket.Cost = v
	}
	if v, err := strconv.ParseInt(values["amount"][0], 10, 0); err == nil {
		ticket.Amount = v
	}
	ticket.EventId = values["eventid"][0]
	ticket.Cat = values["cat"][0]

	if ticket, err := ticket.Save(""); err != nil {
		e.Flash.Error("erro ao criar o ticket", ticket)
		return e.Redirect(Tickets.Main)
	}
	e.Flash.Success("Criado com sucesso")
	return e.Redirect(App.Index)
}

func (e Tickets) Remove(id string) revel.Result {
	if gone, _ := new(models.Ticket).RemoveTicket(id); gone {
		e.Flash.Success("Removido com Successo")
	} else {
		e.Flash.Error("erro ao deletar")
	}
	return e.Redirect(App.Index)
}
